# Test Architecture with Protractor

This project is a sample application to show an architecture proposal for writing end-to-end tests with the Protractor framework in a readable, reliable, and maintainable way.

## Pre-requirements

- [MongoDB](./MONGODB_INSTALLATION.md)
- [git](https://git-scm.com/downloads)
- [Node.js](https://nodejs.org/) (version 10 or greater)
- [NPM](http://npmjs.com) (version 6 or greater)
- [Chrome](https://www.google.com/chrome/) browser
- [Firefox](https://www.mozilla.org/pt-PT/firefox/new/) browser

> To check the Node.js and NPM versions installed on your computer, run `node -v && npm -v`. You should see something like this:

```
v10.16.3
6.9.0
```

> If you don't have Node.js installed, use the above link to download and install it. NPM is automatically installed together with Node.js.

## Installation

[Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) the project to your computer. Run `git clone https://gitlab.com/wlsf82/protractor-course-udemy`.

Inside the directory of the cloned project, run `npm i` to install the project dependencies.

## Starting the app

First, follow to instructions to install and start MongoDB, as described in [this document](./MONGODB_INSTALLATION.md).

Then, in another terminal window, run `npm start`.

Finally, visit http://localhost:4001/ on Chrome or Firefox and you should see the sample application.

## Running the tests

Run `npm t` to run the tests in headless mode. Both Chrome and Firefox tests will be run, sequentially.

Run `npm run test:chrome` to run the tests only on Chrome browser. Tests will be run in headless mode.

Run `npm run test:firefox` to run the tests only on Firefox browser. Tests will be run in headless mode.

## Feedback

Your feedback is very important for this project. If you have any doubts or suggestions open an issue, and I'll look into it as soon as possible.
