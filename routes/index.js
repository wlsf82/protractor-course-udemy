const router = require('express').Router()

const Item = require('../models/item')

router.get('/', async (req, res) => {
  const items = await Item.find({})
  res.render('index', { items })
})

router.get('/items/create', async (req, res) => {
  res.render('create')
})

router.post('/items/create', async (req, res) => {
  const { title, description, imageUrl } = req.body

  const newItem = new Item({
    title,
    description,
    imageUrl
  })

  newItem.validateSync()

  if (newItem.errors) {
    res.status(400).render('create', { newItem })
  } else {
    await newItem.save()
    res.redirect('/')
  }
})

router.get('/items/:id', async (req, res) => {
  const item = await Item.findById({ _id: req.params.id })

  res.render('item', { item })
})

router.get('/items/:id/delete', async (req, res) => {
  res.render('delete')
})

router.post('/items/:id/delete', async (req, res) => {
  Item.remove({ _id: req.params.id }, (error) => {
    if (error) return res.send(error)
    res.redirect('/')
  })
})

router.get('/items/:id/update', async (req, res) => {
  const item = await Item.findById({ _id: req.params.id })

  res.render('update', { item })
})

router.post('/items/:id/update', async (req, res) => {
  const item = await Item.findById({ _id: req.params.id })
  const { title, description, imageUrl } = req.body

  const updatedItem = new Item({
    title,
    description,
    imageUrl
  })

  updatedItem.validateSync()

  if (updatedItem.errors) {
    if (updatedItem.errors.title) {
      item.title = ''
    }
    if (updatedItem.errors.imageUrl) {
      item.imageUrl = ''
    }
    res.status(400).render('update', { updatedItem, item })
  } else {
    await Item.findOneAndUpdate({ _id: req.params.id }, req.body, error => {
      if (error) return res.send(error)
      res.redirect(`/items/${req.params.id}`)
    })
  }
})

module.exports = router
