#!/usr/bin/env node

const faker = require('faker')

const { mongoose, databaseUrl, options } = require('../database')
const Item = require('../models/item')

const seed = async () => {
  await mongoose.connect(databaseUrl, options)
  const item1 = {
    title: faker.lorem.word(),
    description: `${faker.lorem.words(9)}.`,
    imageUrl: faker.image.image()
  }
  const item2 = {
    title: faker.lorem.word(),
    description: `${faker.lorem.words(9)}.`,
    imageUrl: faker.image.image()
  }
  const item3 = {
    title: faker.lorem.word(),
    description: `${faker.lorem.words(9)}.`,
    imageUrl: faker.image.image()
  }
  const item4 = {
    title: faker.lorem.word(),
    description: `${faker.lorem.words(9)}.`,
    imageUrl: faker.image.image()
  }
  const item5 = {
    title: faker.lorem.word(),
    description: `${faker.lorem.words(9)}.`,
    imageUrl: faker.image.image()
  }
  const item6 = {
    title: faker.lorem.word(),
    description: `${faker.lorem.words(9)}.`,
    imageUrl: faker.image.image()
  }

  await Item.create(item1)
  await Item.create(item2)
  await Item.create(item3)
  await Item.create(item4)
  await Item.create(item5)
  await Item.create(item6)
}

seed()
  .then(() => {
    console.log('Seeded database sucessfully')
    process.exit(0)
  })
  .catch(err => {
    console.log('Database seed did not succeeded')
    process.exit(1)
    throw err
  })
