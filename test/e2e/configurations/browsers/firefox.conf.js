const sharedConfig = require('./sharedConfig')

module.exports.config = require('../createConfig.js')({
  capabilities: {
    browserName: 'firefox',
    marionette: true,
    'moz:firefoxOptions': {
      args: [sharedConfig.HEADLESS]
    }
  }
})
