const sharedConfig = require('./sharedConfig')

module.exports.config = require('../createConfig.js')({
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: [sharedConfig.HEADLESS]
    }
  }
})
