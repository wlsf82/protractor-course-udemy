const SpecReporter = require('jasmine-spec-reporter').SpecReporter

module.exports = providedConfig => {
  const defaultConfig = {
    baseUrl: 'http://localhost:4001/',
    specs: ['../../specs/*.spec.js'],
    onPrepare: () => {
      browser.waitForAngularEnabled(false)
      jasmine.getEnv().addReporter(new SpecReporter({
        displayFailuresSummary: true,
        displayFailedSpec: true,
        displaySuiteNumber: true,
        displaySpecDuration: true
      }))
      afterEach(() => {
        browser.manage().deleteAllCookies()
        return browser.executeScript('sessionStorage.clear(); localStorage.clear();')
      })
    }
  }

  return Object.assign(
    {},
    defaultConfig,
    providedConfig
  )
}