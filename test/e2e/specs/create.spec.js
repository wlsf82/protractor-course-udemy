const faker = require('faker')
const helper = require('protractor-helper')

const CreateItemPage = require('../page-objects/createItem')
const HomePage = require('../page-objects/home')

describe("given I'm at the relative url 'items/create'", () => {
  let createItemPage

  beforeEach(() => {
    createItemPage = new CreateItemPage()

    createItemPage.visit()
  })

  describe('then the image preview element', () => {
    it("has no value in the 'src' attribute", () => {
      expect(createItemPage.preview.image.getAttribute('src'))
        .toEqual('')
    })

    it("doesn't render while the image URL filed is empty", () => {
      helper.waitForElementNotToBeVisible(createItemPage.preview.image)
    })
  })

  describe('when I fill the image URL field with a valid image path', () => {
    const IMAGE_URL = faker.image.image()

    beforeEach(() => createItemPage.form.fillImageUrlField(IMAGE_URL))

    describe('then the image preview element', () => {
      it("uses the provided value in the 'src' attribute, and it is rendered", () => {
        helper.waitForElementVisibility(createItemPage.preview.image)
        expect(createItemPage.preview.image.getAttribute('src'))
          .toEqual(IMAGE_URL)
      })
    })
  })

  describe('when I click the back button', () => {
    beforeEach(() => createItemPage.form.goBack())

    it("then I'm redirected to the homepage", () => {
      helper.waitForUrlToBeEqualToExpectedUrl(browser.baseUrl)
    })
  })

  describe("when I click the header's logo", () => {
    beforeEach(() => createItemPage.header.clickLogo())

    it("then I'm redirected to the homepage", () => {
      helper.waitForUrlToBeEqualToExpectedUrl(browser.baseUrl)
    })
  })

  describe('when I submit the form without filling any field', () => {
    beforeEach(() => createItemPage.form.submit())

    it('then the mandatory fields boarder get red, meaning error', () => {
      helper.waitForElementVisibility(createItemPage.form.errors.last())

      expect(createItemPage.form.errors.count()).toBe(2)
    })
  })

  describe('when I fill the form with valid data and submit', () => {
    const newItem = {
      title: faker.lorem.word(),
      description: `${faker.lorem.words(9)}.`,
      imageUrl: faker.image.image()
    }

    beforeEach(() => createItemPage.form.fillAndSubmit(newItem))

    it("then I'm redirected to the home page and an item with the provided title is displayed", () => {
      const homePage = new HomePage()

      helper.waitForUrlToBeEqualToExpectedUrl(browser.baseUrl)
      helper.waitForTextToBePresentInElement(homePage.items.parentElement, newItem.title)
    })
  })

  describe('when I fill only the mandatory and submit', () => {
    const newItem = {
      title: faker.lorem.word(),
      imageUrl: faker.image.image()
    }

    beforeEach(() => createItemPage.form.fillAndSubmit(newItem))

    it("then I'm redirected to the home page and an item with the provided title is displayed", () => {
      const homePage = new HomePage()

      helper.waitForUrlToBeEqualToExpectedUrl(browser.baseUrl)
      helper.waitForTextToBePresentInElement(homePage.items.parentElement, newItem.title)
    })
  })
})
