const helper = require('protractor-helper')

const HomePage = require('../page-objects/home')

describe("given I'm at the home page", () => {
  let homePage

  beforeEach(() => {
    homePage = new HomePage()

    homePage.visit()
  })

  describe("when I click the 'add new item' button", () => {
    beforeEach(() => homePage.navigation.clickAddNewItem())

    it("then I'm redirected to the relative URL 'items/create'", () => {
      helper.waitForUrlToBeEqualToExpectedUrl(`${browser.baseUrl}items/create`)
    })
  })
})
