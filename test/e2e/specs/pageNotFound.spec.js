const helper = require('protractor-helper')

const PageNotFound = require('../page-objects/pageNotFound')

describe("given I visit an unnexistent page (e.g. '/foo')", () => {
  let pageNotFound

  beforeEach(() => {
    pageNotFound = new PageNotFound()

    pageNotFound.visit()
  })

  it('then I see a page not found heading', () => {
    helper.waitForTextToBePresentInElement(pageNotFound.main.heading, 'Page not found')
  })
})
