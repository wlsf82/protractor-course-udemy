const helper = require('protractor-helper')

const HomePage = require('../page-objects/home')
const ItemPage = require('../page-objects/item')

describe("given I hover and click the view button of the first item at the home page", () => {
  beforeEach(() => {
    const homePage = new HomePage()

    homePage.visit()

    homePage.items.hoverAndClickViewButtonOfFirstItem()
  })

  it('then I see a button to go to the update item page', () => {
    const itemPage = new ItemPage()

    helper.waitForElementVisibility(itemPage.navigation.updateButton)
  })
})
