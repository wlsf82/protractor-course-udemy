const BasePage = require('./basePage')

const NavigationComponent = require('./components/item/navigation')

class Item extends BasePage {
  constructor (relativeUrl) {
    super()

    this.relativeUrl = relativeUrl

    this.navigation = new NavigationComponent()
  }
}

module.exports = Item
