class BasePage {
  visit (relativeUrl = this.relativeUrl) {
    browser.get(relativeUrl)
  }
}

module.exports = BasePage
