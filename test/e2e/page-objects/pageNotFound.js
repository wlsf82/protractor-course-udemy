const BasePage = require('./basePage')

const MainComponent = require('./components/page-not-found/index')

class PageNotFound extends BasePage {
  constructor () {
    super()

    this.relativeUrl = '/foo'

    this.main = new MainComponent()
  }
}

module.exports = PageNotFound
