const BasePage = require('./basePage')

const ItemsComponent = require('./components/items')
const HomeNavigationComponent = require('./components/home-navigation')

class Home extends BasePage {
  constructor () {
    super()

    this.relativeUrl = ''

    this.items = new ItemsComponent()
    this.navigation = new HomeNavigationComponent()
  }
}

module.exports = Home
