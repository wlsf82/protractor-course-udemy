const selectors = require('../selectors')

class Navigation {
  constructor () {
    this.parentElement = element(by.className(selectors.CLASS.SINGLE_ITEM_NAVIGATION))

    this.updateButton = this.parentElement.element(by.css(selectors.CSS.ITEM_UPDATE_BUTTON))
  }
}

module.exports = Navigation
