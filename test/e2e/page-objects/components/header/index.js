const helper = require('protractor-helper')

const selectors = require('../selectors')

class Header {
  constructor () {
    this.parentElement = element(by.id(selectors.ID.TITLE_CONTAINER))

    this.logo = this.parentElement.element(by.css(selectors.CSS.IMAGE))
  }

  clickLogo() {
    helper.click(this.logo)
  }
}

module.exports = Header
