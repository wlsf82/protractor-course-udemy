const selectors = require('../selectors')

class PageNotFound {
  constructor () {
    this.parentElement = element(by.id(selectors.ID.PAGE_NOT_FOUND))

    this.heading = this.parentElement.element(by.css(selectors.CSS.HEADING_1))
  }
}

module.exports = PageNotFound
