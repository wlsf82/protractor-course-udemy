const ID = {
  CREATED_IMAGE: 'created-image',
  DESCRIPTION_INPUT: 'description-input',
  IMAGE_URL_INPUT: 'imageUrl-input',
  ITEMS_CONTAINER: 'items-container',
  NAVIGATION: 'navigation',
  PAGE_NOT_FOUND: 'page-not-found',
  SUBMIT_BUTTON: 'submit-button',
  TITLE_CONTAINER: 'title-container',
  TITLE_INPUT: 'title-input'
}

const CLASS = {
  BACK_ARROW: 'back-arrow',
  CREATE_BUTTON: 'create-button',
  CREATE_CONTAINER: 'create-container',
  CREATE_ITEM_CONTAINER: 'create-item-form-container',
  ERROR: 'error',
  FORM_NAVIGATION: 'form-navigation',
  IMAGE_PREVIEW_CONTAINER: 'image-preview-container',
  ITEM_CARD: 'item-card',
  ITEM_VIEW: 'item-view',
  SINGLE_ITEM_NAVIGATION: 'single-item-navigation'
}

const CSS = {
  BACK_ARROW: `.${CLASS.FORM_NAVIGATION} .${CLASS.BACK_ARROW}`,
  HEADING_1: 'h1',
  ITEM_UPDATE_BUTTON: '[data-test-id="item-update-btn"]',
  IMAGE: 'img',
  IMAGE_PREVIEW_CONTAINER: `.${CLASS.CREATE_CONTAINER} .${CLASS.IMAGE_PREVIEW_CONTAINER}`,
  ITEM_VIEW: `#${ID.ITEMS_CONTAINER} .${CLASS.ITEM_CARD} .${CLASS.ITEM_VIEW}`,
  FORM: `.${CLASS.CREATE_CONTAINER} .${CLASS.CREATE_ITEM_CONTAINER} form`
}

module.exports = {
  CLASS,
  CSS,
  ID
}
