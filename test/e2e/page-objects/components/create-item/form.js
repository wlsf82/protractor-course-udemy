const helper = require('protractor-helper')

const selectors = require('../selectors')

class Form {
  constructor () {
    this.parentElement = element(by.css(selectors.CSS.FORM))

    this.titleField = this.parentElement.element(by.id(selectors.ID.TITLE_INPUT))
    this.descriptionField = this.parentElement.element(by.id(selectors.ID.DESCRIPTION_INPUT))
    this.imageUrlField = this.parentElement.element(by.id(selectors.ID.IMAGE_URL_INPUT))
    this.backButton = this.parentElement.element(by.css(selectors.CSS.BACK_ARROW))
    this.createButton = this.parentElement.element(by.css(`.${selectors.CLASS.FORM_NAVIGATION} #${selectors.ID.SUBMIT_BUTTON}`))
    this.errors = element.all(by.css(`${selectors.CSS.FORM} .${selectors.CLASS.ERROR}`))
  }

  goBack() {
    helper.click(this.backButton)
  }

  fillAndSubmit (data) {
    if (data.title) {
      this.fillTitleField(data.title)
    }
    if (data.description) {
      this.fillDescriptionField(data.description)
    }
    if (data.imageUrl) {
      this.fillImageUrlField(data.imageUrl)
    }
    this.submit()
  }

  fillTitleField(title) {
    helper.fillFieldWithText(this.titleField, title)
  }

  fillDescriptionField(description) {
    helper.fillFieldWithText(this.descriptionField, description)
  }

  fillImageUrlField(imageUrl) {
    helper.fillFieldWithText(this.imageUrlField, imageUrl)
  }

  submit() {
    helper.click(this.createButton)
  }
}

module.exports = Form
