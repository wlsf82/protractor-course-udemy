const selectors = require('../selectors')

class Preview {
  constructor () {
    this.parentElement = element(by.css(selectors.CSS.IMAGE_PREVIEW_CONTAINER))

    this.image = this.parentElement.element(by.id(selectors.ID.CREATED_IMAGE))
  }
}

module.exports = Preview
