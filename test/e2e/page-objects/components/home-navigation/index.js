const helper = require('protractor-helper')

const selectors = require('../selectors')

class HomeNavigation {
  constructor () {
    this.parentElement = element(by.id(selectors.ID.NAVIGATION))

    this.addNewItemButton = this.parentElement.element(by.className(selectors.CLASS.CREATE_BUTTON))
  }

  clickAddNewItem() {
    helper.click(this.addNewItemButton)
  }
}

module.exports = HomeNavigation
