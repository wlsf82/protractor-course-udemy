const helper = require('protractor-helper')

const selectors = require('../selectors')

class Items {
  constructor () {
    this.parentElement = element(by.id(selectors.ID.ITEMS_CONTAINER))

    this.viewButtonOfFirstItem = element.all(by.css(selectors.CSS.ITEM_VIEW)).first()
  }

  hoverAndClickViewButtonOfFirstItem() {
    helper.hoverAndClick(this.viewButtonOfFirstItem)
  }
}

module.exports = Items
