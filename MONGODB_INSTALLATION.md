# MongoDB installation

We will use MongoDB as the data base for the sample application of this course. By reading these intructions you should be able to run MongoDB on your computer (or Mac or Windows environments.)

> In case your computer notifies you about firewall permissions, allow them.

## Mac OSX installation

Open a new terminal session on your prefered command line tool.
Make sure Homebrew is already installed on your computer (Homebrew is the software that your computer uses to install other apps via command line.) We'll use it to install MongoDB.

Run `brew -v` to check which version of Homebrew you're using.

- In case your computer doesn't have Homebrew installed it will output a messge like `command not found`.
- Homebrew installation - Paste the installation command as described on [Homebrew's offical webpage](https://brew.sh/). Type your password if you're asked to do so and wait for the installation to finish.
- MongoDB installation - Paste the following command on your terminal and press return: `brew install mongodb`.

After the installation, create a directory for MongoDB to store its data - This is the directory where the sample application data will be stored.

Paste the following command on your terminal and press return: `mkdir -p ~/data/db`.

> This command will create the directory on your user's directory.

To start the Mongo process, run the following command: `mongod --dbpath ~/data/db`

> It's required that the database is already running before starting the sample application that will be used during this course.

> In case you're asked that the `mongod` application accepts network connections, choose that you agree.

If Mongo start correctly you should see a message like the one below:

`connection accepted from ... #1 (1 connection now open)`

> To end the Mongo process press CTRL+C in the terminal window where it is running.

## Windows installation

Download - Open the [downloads MongoDB page](https://www.mongodb.com/download-center) and download the most recent "Community Server" version for Windows.

Installation - Open the downloaded file and follow its instructions.

After the installation, create a directory for MongoDB to store its data - This is the directory where the sample application data will be stored.

Open powershell, paste the following command and press return: `md \data\db`.

To start the Mongo process, run the following command: `& 'C:\Program Files\MongoDB\Server\X.X\bin\mongod.exe'` (Replace X.X with the version that you have downloaded.)

If Mongo start correctly you should see a message like the one below:

`connection accepted from ... #1 (1 connection now open)`

> To end the Mongo process press CTRL+C in the terminal window where it is running.
